var _ = require('underscore');

var Logger = require('./logger');

const LOG_LEVEL_EMERGENCY = 0;
const LOG_LEVEL_ALERT = 1;
const LOG_LEVEL_CRITICAL = 2;
const LOG_LEVEL_ERROR = 3;
const LOG_LEVEL_WARNING = 4;
const LOG_LEVEL_NOTICE = 5;
const LOG_LEVEL_INFO = 6;
const LOG_LEVEL_DEBUG = 7;

function Log(options) {
    this.options = options;
    this.logger = new Logger();
    if (typeof options.appname === "string") {
        this.logger.SetAppName(options.appname);
    }
    if (typeof options.referrer === "string") {
        this.logger.SetReferrer(options.referrer);
    }
}

Log.prototype.emergency = function () {
    this.logger.Write(LOG_LEVEL_EMERGENCY, arguments);
}

Log.prototype.alert = function () {
    this.logger.Write(LOG_LEVEL_ALERT, arguments);
}

Log.prototype.critical = function () {
    this.logger.Write(LOG_LEVEL_CRITICAL, arguments);
}

Log.prototype.error = function () {
    this.logger.Write(LOG_LEVEL_ERROR, arguments);
}

Log.prototype.warning = function () {
    this.logger.Write(LOG_LEVEL_WARNING, arguments);
}

Log.prototype.notice = function () {
    this.logger.Write(LOG_LEVEL_NOTICE, arguments);
}

Log.prototype.info = function () {
    this.logger.Write(LOG_LEVEL_INFO, arguments);
}

Log.prototype.debug = function () {
    this.logger.Write(LOG_LEVEL_DEBUG, arguments);
}

Log.prototype.child = function (options) {
    options = _.extend(this.options, options);
    return new Log(options);
}

module.exports = function (options) {
    return new Log(options);
};