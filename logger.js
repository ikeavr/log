function Logger(){
    this.appName = "defaultAppName";
}

Logger.prototype.SetAppName = function (name) {
    this.appName = name;
}

Logger.prototype.SetReferrer = function (name) {
    this.referrer = name;
}

Logger.prototype.GetAppName = function () {
    return this.appName;
}

Logger.prototype.Write = function (level, arguments) {
    var message = "";
    try {
        for (var i=0; i < arguments.length; i++) {
            var append = arguments[i];
            if (typeof append === "object"){
                append = JSON.stringify(append);
            }
            message += append + " ";
        }
        var j = {
            level: level,
            message: message,
            appname: this.GetAppName(),
            referrer: this.referrer
        }
        console.log(JSON.stringify(j));
    } catch(err) {
        console.log(err);
    }
}

module.exports = Logger;